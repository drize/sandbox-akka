package com.sandbox.actors;

import akka.actor.*;
import akka.util.Timeout;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class ActorMailBoxTest {

    /**
     * How the Mailbox Type is Selected
     * When an actor is created, the ActorRefProvider first determines the dispatcher which will execute it. Then the mailbox is determined as follows:
     * <p>
     * 1 If the actor’s deployment configuration section contains a mailbox key then that names a configuration section describing the mailbox type to be used.
     * 2 If the actor’s Props contains a mailbox selection—i.e. withMailbox was called on it—then that names a configuration section describing the mailbox type to be used.
     * 3 If the dispatcher’s configuration section contains a mailbox-type key the same section will be used to configure the mailbox type.
     * 4 If the actor requires a mailbox type as described above then the mapping for that requirement will be used to determine the mailbox type to be used; if that fails then the dispatcher’s requirement—if any—will be tried instead.
     * 5 If the dispatcher requires a mailbox type as described above then the mapping for that requirement will be used to determine the mailbox type to be used.
     * 6 The default mailbox akka.actor.default-mailbox will be used.
     * Default Mailbox
     * When the mailbox is not specified as described above the default mailbox is used.
     * By default it is an unbounded mailbox, which is backed by a @java.util.concurrent.ConcurrentLinkedQueue.
     */

    public static final Timeout SECCOND_TIMEOUT = Timeout.apply(1, TimeUnit.SECONDS);
    private ActorSystem actorSystem;

    @Before
    public void before() throws InterruptedException, TimeoutException {
        actorSystem = ActorSystem.create("mysys");

    }

    /**
     * Create actor with mailbox from deployment configuration
     *
     * @throws InterruptedException
     * @throws TimeoutException
     */
    @Test
    public void testActor() throws InterruptedException, TimeoutException {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = actorSystem.actorOf(
                Props.create(PrinterActor.class),
                "myprioactor");
        inbox.watch(actor);
        for (Object msg : new Object[]{"lowpriority", "lowpriority",
                "highpriority", "pigdog", "pigdog2", "pigdog3", "highpriority",
                PoisonPill.getInstance()}) {
            inbox.send(actor, msg);
        }
        Thread.sleep(1000);

    }

    /**
     * Create actor with mailbox in Props
     *
     * @throws InterruptedException
     * @throws TimeoutException
     */
    @Test
    public void testWithMailboxActor() throws InterruptedException, TimeoutException {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = actorSystem.actorOf(
                Props.create(PrinterActor.class).withMailbox("prio-mailbox"),
                "myprioactor1");
        inbox.watch(actor);
        for (Object msg : new Object[]{"lowpriority", "lowpriority",
                "highpriority", "pigdog", "pigdog2", "pigdog3", "highpriority",
                PoisonPill.getInstance()}) {
            inbox.send(actor, msg);
        }
        Thread.sleep(1000);

    }

    /**
     * Create actor with mailbox in Props
     *
     * @throws InterruptedException
     * @throws TimeoutException
     */
    @Test
    public void testWithMyMailboxActor() throws InterruptedException, TimeoutException {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = actorSystem.actorOf(
                Props.create(PrinterActor.class).withMailbox("my-mailbox"),
                "myprioactor1");
        inbox.watch(actor);
        for (Object msg : new Object[]{"lowpriority", "lowpriority",
                "highpriority", "pigdog", "pigdog2", "pigdog3", "highpriority",
                PoisonPill.getInstance()}) {
            inbox.send(actor, msg);
        }
        Thread.sleep(1000);

    }

}
