package com.sandbox.actors.routers;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.junit.Test;

import com.sandbox.actors.PrinterActor;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Inbox;
import akka.actor.Props;
import akka.routing.FromConfig;
import akka.routing.GetRoutees;
import akka.routing.Routees;
import akka.util.Timeout;

public class SimpleRouterActorTest {

	public static final String ROUTER_CONFIGURATION = "akka.actor.deployment {\n"
			+ "\t/broadcast-router {\n"
			+ "\t\trouter = broadcast-pool\n"
			+ "\t\tnr-of-instances = 5\n"
			+ "\t}\n"
			+ "pool-dispatcher {\n"
			+ "      fork-join-executor.parallelism-min = 5\n"
			+ "      fork-join-executor.parallelism-max = 5\n"
			+ "    }"
			+ "}";

	@Test
	public void testCustomRouting() throws InterruptedException, TimeoutException {
		ActorSystem actorSystem = ActorSystem.create("testsys");
		Inbox inbox = Inbox.create(actorSystem);
		ActorRef router = actorSystem.actorOf(Props.create(SimpleRouterActor.class));
		inbox.watch(router);
		IntStream.range(0, 10).forEach(i -> {
			router.tell("message-" + i, ActorRef.noSender());
		});
		Thread.sleep(1000);
		inbox.send(router, GetRoutees.getInstance());
		Routees receive = (Routees) inbox.receive(Timeout.apply(1, TimeUnit.SECONDS).duration());
		Assert.assertEquals(5, receive.routees().size());
		actorSystem.terminate();
	}

	@Test
	public void testRouting() throws InterruptedException, TimeoutException {
		Config parsedConfig = ConfigFactory.parseString(ROUTER_CONFIGURATION);
		Config load = ConfigFactory.load().withFallback(parsedConfig);
		ActorSystem actorSystem = ActorSystem.create("testsys", load);
		Inbox inbox = Inbox.create(actorSystem);
		ActorRef router = actorSystem.actorOf(FromConfig.getInstance().props(Props.create(PrinterActor.class)), "broadcast-router");
		inbox.watch(router);
		IntStream.range(0, 10).forEach(i -> {
			router.tell("message-" + i, ActorRef.noSender());
		});
		Thread.sleep(1000);
		inbox.send(router, GetRoutees.getInstance());
		Routees receive = (Routees) inbox.receive(Timeout.apply(1, TimeUnit.SECONDS).duration());
		Assert.assertEquals(5, receive.routees().size());
		actorSystem.terminate();
	}

	@Test
	public void testRoutingFromConf() throws InterruptedException, TimeoutException {
		Config load = ConfigFactory.load();
		ActorSystem actorSystem = ActorSystem.create("mysys", load);
		Inbox inbox = Inbox.create(actorSystem);
		ActorRef router = actorSystem.actorOf(FromConfig.getInstance().props(Props.create(PrinterActor.class)), "poolWithDispatcher");
		inbox.watch(router);
		IntStream.range(0, 10).forEach(i -> {
			router.tell("message-" + i, ActorRef.noSender());
		});
		Thread.sleep(1000);
		inbox.send(router, GetRoutees.getInstance());
		Routees receive = (Routees) inbox.receive(Timeout.apply(1, TimeUnit.SECONDS).duration());
		Assert.assertEquals(5, receive.routees().size());
		actorSystem.terminate();
	}






}