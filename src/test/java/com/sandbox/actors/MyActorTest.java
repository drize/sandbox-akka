package com.sandbox.actors;

import akka.actor.*;
import akka.pattern.AskTimeoutException;
import akka.util.Timeout;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import scala.concurrent.Await;
import scala.concurrent.Awaitable;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static akka.pattern.PatternsCS.gracefulStop;


public class MyActorTest {

    public static final Timeout SECCOND_TIMEOUT = Timeout.apply(1, TimeUnit.SECONDS);
    private ActorSystem actorSystem;
    private Inbox inbox;
    private ActorRef actorRef;

    @Before
    public void before() throws InterruptedException, TimeoutException {
        actorSystem = ActorSystem.create("mysys");
        inbox = Inbox.create(actorSystem);
        actorRef = actorSystem.actorOf(Props.create(MyActor.class), "myactor");
        inbox.watch(actorRef);
    }


    @Test
    public void testActor() throws InterruptedException, TimeoutException {
        inbox.send(actorRef, "first message");
        Assert.assertEquals("receive", inbox.receive(Duration.create(1, TimeUnit.SECONDS)));
        actorSystem.actorSelection("/user/myactor").tell("from path", ActorRef.noSender());
        Thread.sleep(1000);
        Awaitable<Terminated> ready = actorSystem.terminate();

    }

    @Test
    public void testActorIdentity() throws InterruptedException, TimeoutException {
        inbox.send(actorRef, "first message");
        Assert.assertEquals("receive", inbox.receive(Duration.create(1, TimeUnit.SECONDS)));
        actorSystem.actorSelection("/user/myactor").tell(new Identify(1), inbox.getRef());
        Assert.assertEquals(actorRef, ((ActorIdentity) inbox.receive(Duration.create(1, TimeUnit.SECONDS))).getActorRef().get());
        Thread.sleep(1000);
        Awaitable<Terminated> ready = actorSystem.terminate();

    }

    @Test
    public void testActorResolveOne() throws Exception {
        Future<ActorRef> actorRefFuture = actorSystem.actorSelection("/user/myactor").resolveOne(SECCOND_TIMEOUT);
        Assert.assertEquals(actorRef, Await.result(actorRefFuture, SECCOND_TIMEOUT.duration()));
        Thread.sleep(1000);
        actorSystem.terminate();
    }

    @Test
    public void testActorTimeout() throws Exception {
        actorRef.tell("initial message", ActorRef.noSender());
        Thread.sleep(5000);
        actorSystem.terminate();
    }

    @Test
    public void testActorScheduler() throws Exception {
        actorSystem.actorOf(Props.create(ActorWithTimer.class), "actorwithtimer");
        Thread.sleep(5000);
        actorSystem.terminate();
    }

    @Test
    public void stopingActors() throws InterruptedException, TimeoutException, ExecutionException {
        ActorRef actor1 = actorSystem.actorOf(Props.create(LifecycledActor.class, LifecycledActor::new));
        actor1.tell(PoisonPill.getInstance(), ActorRef.noSender());// stop actor when process message
        ActorRef actor2 = actorSystem.actorOf(Props.create(LifecycledActor.class, LifecycledActor::new));
        actor2.tell(Kill.getInstance(), ActorRef.noSender());//stop and throw a ActorKilledException, triggering a failure
        ActorRef actor3 = actorSystem.actorOf(Props.create(LifecycledActor.class, LifecycledActor::new));
        actorSystem.stop(actor3);// stop actor and drop mailbox

        //gracefully stop
        try {
            CompletionStage<Boolean> stopped =
                    gracefulStop(actorRef, Duration.create(1, TimeUnit.SECONDS), PoisonPill.getInstance());
            stopped.toCompletableFuture().get(2, TimeUnit.SECONDS);
            // the actor has been stopped
        } catch (AskTimeoutException e) {
            // the actor wasn't stopped within 5 seconds
        }
        Thread.sleep(2000);
        actorSystem.terminate();
    }

    @Test
    public void testBecome() throws TimeoutException {
        ActorRef actor = actorSystem.actorOf(Props.create(HotSwapActor.class));
        inbox.watch(actor);
        inbox.send(actor, "foo");
        inbox.send(actor, "foo");
        Assert.assertEquals("I am already angry?", inbox.receive(SECCOND_TIMEOUT.duration()));
        inbox.send(actor, "bar");
        inbox.send(actor, "bar");
        Assert.assertEquals("I am already happy :-)", inbox.receive(SECCOND_TIMEOUT.duration()));
    }

}
