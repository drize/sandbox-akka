package com.sandbox.actors;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Inbox;
import akka.actor.Props;
import akka.util.Timeout;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class ActorStashTest {

    public static final Timeout SECCOND_TIMEOUT = Timeout.apply(1, TimeUnit.SECONDS);
    private ActorSystem actorSystem;

    @Before
    public void before() throws InterruptedException, TimeoutException {
        actorSystem = ActorSystem.create("mysys");

    }

    @Test
    public void testActor() throws InterruptedException, TimeoutException {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = actorSystem.actorOf(
                Props.create(HotSwapActor.class).withDispatcher("akka.actor.my-dispatcher"),
                "myactor");
        inbox.watch(actor);
        inbox.send(actor, "foo");
        inbox.send(actor, "foo");
        Assert.assertEquals("I am already angry?", inbox.receive(SECCOND_TIMEOUT.duration()));
        String doMsg = "do";
        inbox.send(actor, doMsg);

        inbox.send(actor, "bar");
        Assert.assertEquals("Happy to serve : " + doMsg, inbox.receive(SECCOND_TIMEOUT.duration()));
        inbox.send(actor, "bar");
        Assert.assertEquals("I am already happy :-)", inbox.receive(SECCOND_TIMEOUT.duration()));
    }

    @Test
    public void testPinnedDispatchers() throws InterruptedException, TimeoutException {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = actorSystem.actorOf(
                Props.create(HotSwapActor.class), "pinned-myactor");
        inbox.watch(actor);
        inbox.send(actor, "foo");
        inbox.send(actor, "foo");
        Assert.assertEquals("I am already angry?", inbox.receive(SECCOND_TIMEOUT.duration()));

    }

}
