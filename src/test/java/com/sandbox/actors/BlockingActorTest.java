package com.sandbox.actors;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.util.Timeout;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class BlockingActorTest {

    public static final Timeout TIMEOUT = Timeout.apply(10, TimeUnit.SECONDS);
    private ActorSystem actorSystem;

    @Before
    public void before() throws InterruptedException, TimeoutException {
        actorSystem = ActorSystem.create("mysys");

    }

    @Test
    public void testActor() throws InterruptedException, TimeoutException {
        ActorRef actor = actorSystem.actorOf(Props.create(BlockingActor.class),
                "blocking-actor");
        actor.tell(123, ActorRef.noSender());
        Thread.sleep(6000);
        actorSystem.terminate();
    }

}
