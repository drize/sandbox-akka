package com.sandbox.actors.futures;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Assert;
import org.junit.Test;

import com.sandbox.actors.PrinterActor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Cancellable;
import akka.actor.Props;
import akka.dispatch.Futures;
import akka.dispatch.OnSuccess;
import akka.pattern.Patterns;
import akka.util.Timeout;
import scala.compat.java8.FutureConverters;
import scala.concurrent.Await;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;

public class FuturesTest {

	public static final Timeout TIMEOUT = Timeout.apply(1, TimeUnit.SECONDS);

	@Test
	public void test1() throws InterruptedException, TimeoutException {
		ActorSystem actorSystem = ActorSystem.create("futures");
		ActorRef actorRef = actorSystem.actorOf(Props.create(PrinterActor.class), "actor");
		Future<Object> future = Patterns.ask(actorRef, "ask", TIMEOUT);
		Future<Object> objectFuture = Patterns.pipe(future, actorSystem.dispatcher()).pipeTo(actorRef, ActorRef.noSender());
		Await.ready(objectFuture, TIMEOUT.duration());
		Thread.sleep(1000);
	}

	@Test
	public void test2() throws Exception {
		ActorSystem actorSystem = ActorSystem.create("futures");

		Future<String> future = Futures.future(new Callable<String>() {

			@Override
			public String call() throws Exception {
				return "Hello" + " World";
			}
		}, actorSystem.dispatcher());
		String result = Await.result(future, TIMEOUT.duration());
		Assert.assertEquals("Hello World", result);
		Thread.sleep(1000);
	}



	@Test
	public void test3() throws Exception {
		ActorSystem actorSystem = ActorSystem.create("futures");

		Future<String> future = Futures.future(new Callable<String>() {

			@Override
			public String call() throws Exception {
				return "Hello" + " World";
			}
		}, actorSystem.dispatcher());

		Future<Integer> integerFuture = future.map(s -> s.length(), actorSystem.dispatcher());

		integerFuture.onSuccess(new PrintResult<Integer>(), actorSystem.dispatcher());
		int length = Await.result(integerFuture, TIMEOUT.duration());
		Assert.assertEquals(11, length);
	}

	/***
	 * In this example Scala Future is converted to CompletionStage just like Akka does.
	 * The completion is delayed: we are calling thenApply multiple times on a not yet complete CompletionStage, then complete the Future.
		 First thenApply is actually performed on scala-java8-compat instance and computational stage (lambda)
		 execution is delegated to default Java thenApplyAsync which is executed on ForkJoinPool.commonPool().
	 */
	@Test
	public void test4() throws Exception {
		ActorSystem actorSystem = ActorSystem.create("futures");
		final ExecutionContext ec = actorSystem.dispatcher();
		final CountDownLatch countDownLatch = new CountDownLatch(1);

		Future<String> scalaFuture = Futures.future(() -> {
			assertThat(Thread.currentThread().getName(), containsString("akka.actor.default-dispatcher"));
			countDownLatch.await(); // do not complete yet
			return "hello";
		}, ec);

		/*convert to java stage and execute it in separate ForkJoinPool.commonPool*/
		CompletionStage<String> fromScalaFuture = FutureConverters.toJava(scalaFuture)
				.thenApply(s -> { // 1
					assertThat(Thread.currentThread().getName(), containsString("ForkJoinPool.commonPool"));
					return s;
				})
				.thenApply(s -> { // 2
					assertThat(Thread.currentThread().getName(), containsString("ForkJoinPool.commonPool"));
					return s;
				})
				.thenApply(s -> { // 3
					assertThat(Thread.currentThread().getName(), containsString("ForkJoinPool.commonPool"));
					return s;
				});

		countDownLatch.countDown(); // complete scalaFuture
	}




	public final static class PrintResult<T> extends OnSuccess<T> {
		@Override public final void onSuccess(T t) {
			System.out.println(t);
		}
	}

}
