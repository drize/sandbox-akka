package com.sandbox.actors;

import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.util.Timeout;
import scala.PartialFunction;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class LifecycledActor extends AbstractActor {
    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    @Override
    public void preStart() throws Exception {
        log.info("PreStart");
    }

    @Override
    public void postStop() throws Exception {
        log.info("post Stop"); // all children sopped and sent as terminate message, then call post Stop
    }

    @Override
    public void preRestart(Throwable reason, Optional<Object> message) throws Exception {
        log.info("PreRestart");// before restart on old instance
    }

    @Override
    public void postRestart(Throwable reason) throws Exception {
        log.info("Prost stop");// after restart on new instance
    }

    public Receive createReceive() {
        return receiveBuilder()
                .match(String.class, s -> {
                    log.info("Receiving String message : {}", s);
                })
                .match(ReceiveTimeout.class, tout -> {
                    log.info("Receiving timeout : {}", tout);
                })
                .match(Terminated.class, t ->{
                    log.info("Receiving terminated from  {}", t.getActor());
                }).build();
    }

}
