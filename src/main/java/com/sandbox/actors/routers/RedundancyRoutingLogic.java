package com.sandbox.actors.routers;

import java.util.ArrayList;
import java.util.List;

import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.RoutingLogic;
import akka.routing.SeveralRoutees;
import scala.collection.immutable.IndexedSeq;

public class RedundancyRoutingLogic implements RoutingLogic{

	private final int nbrCopies;
	private RoundRobinRoutingLogic roundRobinlogic = new RoundRobinRoutingLogic();

	public RedundancyRoutingLogic(int nbrCopies) {
		this.nbrCopies = nbrCopies;
	}

	@Override
	public Routee select(Object message, IndexedSeq<Routee> routees) {
		List<Routee> targets = new ArrayList<>();
		for (int i =0; i < nbrCopies; i++){
			targets.add(roundRobinlogic.select(message, routees));
		}
		return new SeveralRoutees(targets);
	}
}
