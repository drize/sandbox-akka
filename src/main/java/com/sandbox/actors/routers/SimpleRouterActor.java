package com.sandbox.actors.routers;

import static akka.actor.SupervisorStrategy.escalate;
import static akka.actor.SupervisorStrategy.restart;
import static akka.actor.SupervisorStrategy.resume;
import static akka.actor.SupervisorStrategy.stop;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import com.sandbox.actors.PrinterActor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.OneForOneStrategy;
import akka.actor.Props;
import akka.actor.SupervisorStrategy;
import akka.actor.Terminated;
import akka.japi.pf.DeciderBuilder;
import akka.routing.ActorRefRoutee;
import akka.routing.GetRoutees;
import akka.routing.Routee;
import akka.routing.Routees;
import akka.routing.Router;
import scala.concurrent.duration.Duration;

public class SimpleRouterActor extends AbstractActor{


	private Router router;
	{
		List<Routee> routees = new ArrayList<>();
		IntStream.range(0, 5).forEach( i -> {
			ActorRef actorRef = getContext().actorOf(Props.create(PrinterActor.class), "printer-" + i);
			getContext().watch(actorRef);
			routees.add(new ActorRefRoutee(actorRef));
		});
		router = new Router(new RedundancyRoutingLogic(2), routees);
	}



	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(String.class, message -> {
					router.route(message, getSender());
				})
				.match(Terminated.class, message -> {
					router = router.removeRoutee(message.getActor());
					ActorRef actorRef = getContext().actorOf(Props.create(PrinterActor.class), message.getActor().path().name());
					getContext().watch(actorRef);
					router = router.addRoutee(new ActorRefRoutee(actorRef));
				})
				.match(GetRoutees.class, s -> {
					getSender().tell(new Routees(router.routees()), getSelf());
				})
				.build();
	}

	@Override
	public SupervisorStrategy supervisorStrategy() {
		return new OneForOneStrategy(10, Duration.create(1, TimeUnit.MINUTES), DeciderBuilder.
				match(ArithmeticException.class, e -> resume()).
				match(NullPointerException.class, e -> restart()).
				match(IllegalArgumentException.class, e -> stop()).
				matchAny(o -> escalate()).build());
	}
}
