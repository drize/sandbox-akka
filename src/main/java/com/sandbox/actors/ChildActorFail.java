package com.sandbox.actors;

import akka.actor.AbstractActor;
import akka.event.DiagnosticLoggingAdapter;
import akka.event.Logging;

import java.util.Optional;

public class ChildActorFail extends AbstractActor {

    public final DiagnosticLoggingAdapter LOGGER = Logging.getLogger(this);

    @Override
    public void preStart() throws Exception {
        super.preStart();
        LOGGER.info("pre start");
    }

    @Override
    public void preRestart(Throwable reason, Optional<Object> message) throws Exception {
//        super.preRestart(reason, message);
        LOGGER.info("pre restart");
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
        LOGGER.info("post stop");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().match(String.class, s -> {
            LOGGER.info("Error will generate");
            throw new NullPointerException("from " + this);
        }).build();
    }
}
