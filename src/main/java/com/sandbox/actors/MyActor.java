package com.sandbox.actors;

import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.DeciderBuilder;
import akka.util.Timeout;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

import static akka.actor.SupervisorStrategy.*;

public class MyActor extends AbstractActor {
    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    /**
     * Escalate is used if the defined strategy doesn’t cover the exception that was thrown.
     * <p>
     * it is a one-for-one strategy, meaning that each child is treated
     * separately (an all-for-one strategy works very similarly, the only
     * difference is that any decision is applied to all children of the supervisor,
     * not only the failing one). In the above example, 10 and
     * Duration.create(1, TimeUnit.MINUTES) are passed to the maxNrOfRetries and withinTimeRange
     * parameters respectively, which means that the strategy restarts a child up to 10 restarts per minute.
     * The child actor is stopped if the restart count exceeds maxNrOfRetries during the withinTimeRange duration
     */
    private static SupervisorStrategy strategy =
            new OneForOneStrategy(10, Duration.create(1, TimeUnit.MINUTES), DeciderBuilder.
                    match(ArithmeticException.class, e -> resume()).
                    match(NullPointerException.class, e -> restart()).
                    match(IllegalArgumentException.class, e -> stop()).
                    matchAny(o -> escalate()).build());

    @Override
    public void preStart() throws Exception {
        getContext().setReceiveTimeout(Timeout.apply(1, TimeUnit.SECONDS).duration());
    }

    public Receive createReceive() {
        return receiveBuilder()
                .match(String.class, s -> {
                    log.info("Receiving String message : {}", s);
                    getSender().tell("receive", getSelf());
                    ActorRef actorRef = getContext().actorOf(Props.create(ChildActorFail.class, ChildActorFail::new));
                    getContext().watch(actorRef);
                    actorRef.tell(s, getSelf());actorRef.tell(PoisonPill.getInstance(), getSelf());
                })
                .match(ReceiveTimeout.class, tout -> {
                    log.info("Receiving timeout : {}", tout);
                })
                .match(Terminated.class, t ->{
                    log.info("Receiving terminated from  {}", t.getActor());
                }).build();
    }

    @Override
    public void unhandled(Object message) {
        super.unhandled(message);
        log.info("Receiving unhandled message : {}", message);
    }


    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }
}
