package com.sandbox.actors;

import akka.actor.AbstractActorWithTimers;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

public class ActorWithTimer extends AbstractActorWithTimers {

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    private static Object TICK_KEY = "TickKey";

    private static final class FirstTick {
    }

    private static final class Tick {
    }

    public ActorWithTimer() {
        getTimers().startSingleTimer(TICK_KEY, new FirstTick(),
                Duration.create(500, TimeUnit.MILLISECONDS));
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(FirstTick.class, message -> {
                    log.info("Receive first tick");
                    getTimers().startPeriodicTimer(TICK_KEY, new Tick(),
                            Duration.create(2, TimeUnit.SECONDS));
                })
                .match(Tick.class, message -> {
                    log.info("Receive tick");
                })
                .build();
    }
}
