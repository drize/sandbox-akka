package com.sandbox.actors;

import java.util.Optional;

import akka.actor.AbstractLoggingActor;

public class PrinterActor extends AbstractLoggingActor {

    private int processed = 0;

    @Override
    public void preRestart(Throwable reason, Optional<Object> message) throws Exception {
        log().info(getSelf().path().name() + " restarted");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().matchAny(s -> {
            log().info(getSelf().path().name() + " - receive : " + s.toString());
            getSender().tell("return:" + s, getSelf());
            processed++;
            if(processed%5 == 0){
                log().info(getSelf().path().name() + " generate exception");
                throw new IllegalArgumentException("Generated exception");
            }
        }).build();
    }
}
