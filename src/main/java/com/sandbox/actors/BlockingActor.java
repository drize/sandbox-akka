package com.sandbox.actors;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.dispatch.Futures;
import scala.concurrent.ExecutionContext;

public class BlockingActor extends AbstractLoggingActor {

    ExecutionContext context = getContext().getSystem().dispatchers().lookup("akka.actor.my-blocking-dispatcher");

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Integer.class, s -> {
                    log().info("Receive " + s + " in dispatcher : " + getContext().dispatcher().toString());
                    Futures.future(() -> {
                        log().info("start sleep");
                        Thread.sleep(5000);
                        log().info("stop blocking operation");
                        getSelf().tell(String.valueOf(s), ActorRef.noSender());
                        return Void.TYPE;
                    }, context);
                    log().info("Handled " + s + " in dispatcher : " + getContext().dispatcher().toString());
                })
                .match(String.class, s -> {
                    log().info("Print message : " + s + " in dispatcher : " + getContext().dispatcher().toString());
                }).build();
    }
}
