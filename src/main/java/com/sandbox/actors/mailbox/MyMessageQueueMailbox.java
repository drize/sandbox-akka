package com.sandbox.actors.mailbox;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.dispatch.Envelope;
import akka.dispatch.MailboxType;
import akka.dispatch.MessageQueue;
import akka.dispatch.ProducesMessageQueue;
import com.typesafe.config.Config;
import scala.Option;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MyMessageQueueMailbox implements MailboxType, ProducesMessageQueue<MyMessageQueueMailbox.MyMessageQueue> {

    public MyMessageQueueMailbox(ActorSystem.Settings settings, Config config) {
        super();
    }

    @Override
    public MessageQueue create(Option<ActorRef> owner, Option<ActorSystem> system) {
        return new MyMessageQueue();
    }

    public static class MyMessageQueue implements MessageQueue, MyUnboundedMessageQueueSemantic {
        private final Queue<Envelope> queue = new ConcurrentLinkedQueue<>();

        @Override
        public void enqueue(ActorRef receiver, Envelope handle) {
            queue.offer(handle);
        }

        @Override
        public Envelope dequeue() {
            return queue.poll();
        }

        @Override
        public int numberOfMessages() {
            return queue.size();
        }

        @Override
        public boolean hasMessages() {
            return !queue.isEmpty();
        }

        @Override
        public void cleanUp(ActorRef owner, MessageQueue deadLetters) {
            for (Envelope envelope : queue) {
                deadLetters.enqueue(owner, envelope);
            }
        }
    }

}
