package com.sandbox.actors;

import akka.actor.AbstractActor;
import akka.actor.AbstractActorWithStash;
import akka.event.DiagnosticLoggingAdapter;
import akka.event.Logging;

public class HotSwapActor extends AbstractActorWithStash {

    private final DiagnosticLoggingAdapter logger = Logging.getLogger(this);
    private AbstractActor.Receive angry;
    private AbstractActor.Receive happy;

    public HotSwapActor() {
        angry = receiveBuilder()
                .matchEquals("foo", s -> {
                    getSender().tell("I am already angry?", getSelf());
                })
                .matchEquals("bar", s -> {
                    logger.info("Swap to happy");
                    getContext().become(happy);
                    unstashAll();
                })
                .matchAny(s -> {
                    logger.info("Stash " + s);
                    stash();
                }).build();
        happy = receiveBuilder()
                .matchEquals("bar", s -> {
                    getSender().tell("I am already happy :-)", getSelf());
                })
                .matchEquals("foo", s -> {
                    logger.info("Swap to angry");
                    getContext().become(angry);
                })
                .matchAny(s -> {
                    getSender().tell("Happy to serve : " + s, getSelf());
                }).build();
    }


    @Override
    public Receive createReceive() {
        return receiveBuilder().matchEquals("foo", s -> {
            getContext().become(angry);
        }).matchEquals("bar", s -> getContext().become(happy))
                .build();
    }
}
